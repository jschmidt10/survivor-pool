#!/bin/bash
#
# Tests and packs javascript artifacts for AWS Lambda.
#

set -o errexit
set -o pipefail

declare basedir="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

cd ${basedir}/..

buildDir="dist"
buildArtifact="survivorpool-web.zip"

echo "Building core"
cd core/
npm install
npm run-script format
npm test
npm pack

echo "Building web"
cd ../web/
npm install ../core/survivorpool-*.tgz
npm install
npm run-script format
npm test

echo "Packaging web for AWS Lambda"
cd ..
mkdir -p ${buildDir}
cd web/
zip -r ${buildArtifact} index.js lib/ node_modules/

mv ${buildArtifact} ../${buildDir}/

echo "Created ${buildDir}/${buildArtifact}"
