#!/bin/bash
#
# Deploys the UI files to S3. You may need to explicitly make the folder public again.

set -o errexit
set -o pipefail

declare basedir="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

cd ${basedir}/..

if [[ $# -ne 1 ]]
then
  echo "Missing required argument: <environment> (test or prod)"
  exit 1
fi

source ${basedir}/build-env.sh

if [[ ! -f "${DIST_DIR}/ui/public/index.html" ]]
then
  echo "You must first run build-ui.sh"
  exit 1
fi

environment="$1"
rootUrl=""
bucketPath=""

if [[ "${environment}" == "test" ]]
then
  bucketPath="${TEST_S3_PATH}"
  rootUrl="${TEST_API_GATEWAY_URL}"
elif [[ "${environment}" == "prod" ]]
then
  bucketPath="${PROD_S3_PATH}"
  rootUrl="${PROD_API_GATEWAY_URL}"
else
  echo "Unrecognized environment: '${environment}'. Must be 'test' or 'prod'."
  exit 1
fi

echo "Deploying to S3"

if aws s3 sync ${DIST_DIR}/ui/ ${bucketPath} --acl public-read
then
  echo "Success"
fi