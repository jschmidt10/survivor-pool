#!/bin/bash
#
# Checks formatting and builds the ui project.
#

set -o errexit
set -o pipefail

declare basedir="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

cd ${basedir}/..

if [[ $# -ne 1 ]]
then
  echo "Missing required argument: <environment> (test or prod)"
  exit 1
fi

source ${basedir}/build-env.sh

environment="$1"

cd ui

npm install

./scripts/format.sh
./scripts/build.sh
./scripts/replace-url.sh ${environment}

cd ..

uiDistDir="${DIST_DIR}/ui"

[[ -d ${uiDistDir} ]] && rm -rf ./${uiDistDir}

mkdir -p ${DIST_DIR}/ui

cp -R ./ui/dist/* ${DIST_DIR}/ui