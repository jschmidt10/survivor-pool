#!/usr/bin/env bash

TEST_LAMBDA_NAME="survivorpool-test"
TEST_S3_PATH="s3://survivorpool/test"
TEST_API_GATEWAY_URL="https://rz4n4jsmbd.execute-api.us-east-1.amazonaws.com/default/survivorpool-test"

PROD_LAMBDA_NAME="survivorpool"
PROD_S3_PATH="s3://survivorpool/prod"
PROD_API_GATEWAY_URL="https://bnwylviwi2.execute-api.us-east-1.amazonaws.com/prod/survivorpool"

DIST_DIR="dist"