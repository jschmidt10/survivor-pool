'use strict'

const schema = require('./schema')
const validator = require('./validator')

const pool = {}

pool.validate = validator.validate

/*
 * Fetch a pool by name.
 */
pool.fetchByName = async function (config, repo, name) {
  const dynamoResult = await repo.get(schema.getPoolRequest(config, name))

  if (dynamoResult && dynamoResult.Item) {
    return schema.fromDynamoPool(dynamoResult.Item)
  } else {
    return null
  }
}

/*
 * Saves a pool.
 */
pool.save = function (config, repo, newPool) {
  const poolPut = schema.putPoolRequest(config, newPool)
  const listingPut = schema.putListingRequest(config, newPool)

  const poolPromise = repo.put(poolPut)
  const listingPromise = repo.put(listingPut)

  return Promise.all([poolPromise, listingPromise])
}

/*
 * Fetches the listing of all pools.
 */
pool.fetchListing = async function (config, repo) {
  const listAllQuery = schema.getAllListingRequest(config)
  const dynamoResults = await repo.query(listAllQuery)

  if (dynamoResults && dynamoResults.Items) {
    return schema.fromDynamoListing(dynamoResults.Items)
  } else {
    return []
  }
}

/*
 * Deletes a pool.
 */
pool.delete = function (config, repo, poolName) {
  const poolGet = schema.getPoolRequest(config, poolName)
  const poolListingGet = schema.getListingRequest(config, poolName)

  const listingPromise = repo.delete(poolListingGet)
  const poolPromise = repo.delete(poolGet)

  return Promise.all([listingPromise, poolPromise])
}

module.exports = pool
