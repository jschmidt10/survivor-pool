'use strict'

const args = require('../args')

const validator = {}

/*
 * Validates a pool contains all required fields.
 */
validator.validate = function validatePool (pool) {
  args.requireDefined(pool, 'pool')
  args.requireNonBlank(pool.name, 'pool.name')
  args.requireNonBlank(pool.url, 'pool.url')
  args.requireDefined(pool.players, 'pool.players')

  for (const player of pool.players) {
    validatePlayer(player)
  }
}

function validatePlayer (player) {
  args.requireDefined(player, 'player')
  args.requireNonBlank(player.name, 'player.name')
  args.requireDefined(player.contestants, 'player.contestants')

  for (const contestant of player.contestants) {
    args.requireNonBlank(contestant.name, 'player.contestant.name')
  }
}

/*
 * Validates a listing of pools has all required fields.
 */
validator.validateListing = function validateListing (listing) {
  if (listing.pools) {
    for (const pool of listing.pools) {
      args.requireDefined(pool.name, 'listing.pool.name')
      args.requireDefined(pool.url, 'listing.pool.url')
    }
  }
}

module.exports = validator
