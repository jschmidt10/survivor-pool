'use strict'

const args = require('../args')
const config = require('../config')
const validator = require('./validator')

const POOL_ID_PREFIX = 'POOL:'
const LIST_ID_PREFIX = 'POOL_LIST:'
const ID_PARAM = ':id'
const ID_QUERY = `id = ${ID_PARAM}`

const schema = {}

schema.POOL_ID_PREFIX = POOL_ID_PREFIX
schema.LIST_ID_PREFIX = LIST_ID_PREFIX
schema.ID_PARAM = ID_PARAM
schema.ID_QUERY = ID_QUERY

/*
 * Dynamo GET request for a single pool.
 */
schema.getPoolRequest = function (c, name) {
  config.validate(c)
  args.requireDefined(name, 'name')

  return {
    Key: {
      id: POOL_ID_PREFIX + name,
      env: c.env
    },
    TableName: c.table
  }
}

/*
 * Dynamo PUT request for a pool.
 */
schema.putPoolRequest = function (c, pool) {
  config.validate(c)
  validator.validate(pool)

  return {
    TableName: c.table,
    Item: {
      id: POOL_ID_PREFIX + pool.name,
      env: c.env,
      ...pool
    }
  }
}

/*
 * Extracts a pool from a Dynamo result.
 */
schema.fromDynamoPool = function (dynamoPool) {
  validator.validate(dynamoPool)

  return {
    name: dynamoPool.name,
    url: dynamoPool.url,
    players: dynamoPool.players
  }
}

/*
 * Extracts a list of pool metadata from a Dynamo result.
 */
schema.fromDynamoListing = function (dynamoListing) {
  validator.validateListing(dynamoListing)

  if (dynamoListing && dynamoListing.length > 0) {
    return dynamoListing.map((p) => ({
      name: p.name,
      url: p.url
    }))
  } else {
    return []
  }
}

/*
 * Dynamo GET request to fetch all pool listings.
 */
schema.getAllListingRequest = function (c) {
  config.validate(c)

  return {
    TableName: c.table,
    KeyConditionExpression: ID_QUERY,
    ExpressionAttributeValues: {
      [ID_PARAM]: LIST_ID_PREFIX + c.env
    }
  }
}

/*
 * Dynamo GET request for a single pool listing.
 */
schema.getListingRequest = function (c, name) {
  config.validate(c)
  args.requireDefined(name, 'name')

  return {
    TableName: c.table,
    Key: {
      id: LIST_ID_PREFIX + c.env,
      env: name
    }
  }
}

/*
 * Dynamo PUT request for a single pool listing.
 */
schema.putListingRequest = function (c, pool) {
  config.validate(c)
  validator.validate(pool)

  return {
    TableName: c.table,
    Item: {
      id: LIST_ID_PREFIX + c.env,
      env: pool.name,
      name: pool.name,
      url: pool.url
    }
  }
}

module.exports = schema
