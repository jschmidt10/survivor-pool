'use strict'

/*
 * Base class for all application specific errors
 */
class AppError extends Error {
  constructor (message) {
    super(message)
    this.name = this.constructor.name
    Error.captureStackTrace(this, this.constructor)
  }
}

module.exports = AppError
