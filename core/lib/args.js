'use strict'

const AppError = require('./app-error')

const args = {}

class ArgumentError extends AppError {
}

/*
 * Requires that a String variable is not empty
 */
args.requireNonBlank = function (value, name) {
  if ((typeof value) !== 'string' || value.trim().length === 0) {
    throw new ArgumentError(`Expected ${name} to be a non-empty String but received '${value}'`)
  }
}

/*
 * Requires that a value is neither undefined nor null
 */
args.requireDefined = function (value, name) {
  if (value === undefined || value === null) {
    throw new ArgumentError(`Expected ${name} to be defined and non-empty but received '${value}'`)
  }
}

args.ArgumentError = ArgumentError

module.exports = args
