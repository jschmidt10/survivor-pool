'use strict'

const args = require('./args')

/*
 * Mock instance of a Dynamo DB Repo for Survivorpool.
 *
 * Objects are keyed by (id, env)
 */
class MockRepo {
  constructor () {
    this.entries = {}
  }

  put (putRequest) {
    const key = this.getKey(putRequest.Item)
    this.entries[key] = putRequest.Item
    return Promise.resolve({})
  }

  delete (deleteRequest) {
    const key = this.getKey(deleteRequest.Key)
    delete this.entries[key]
    return Promise.resolve({})
  }

  get (getRequest) {
    const key = this.getKey(getRequest.Key)
    const value = this.entries[key]

    if (value === undefined) {
      return Promise.resolve(null)
    } else {
      return Promise.resolve({ Item: value })
    }
  }

  query (queryRequest) {
    const results = []
    const query = queryRequest.ExpressionAttributeValues

    if (!this.isEmpty(query)) {
      for (const itemKey in this.entries) {
        const item = this.entries[itemKey]
        if (this.matchesQuery(item, query)) {
          results.push(item)
        }
      }
    }

    return Promise.resolve({ Items: results })
  }

  isEmpty (obj) {
    return Object.keys(obj).length === 0
  }

  matchesQuery (item, query) {
    let match = true

    for (const queryKey in query) {
      // strip leading ':' for item key
      const itemKey = queryKey.substring(1)
      if (item[itemKey] !== query[queryKey]) {
        match = false
      }
    }

    return match
  }

  getKey (item) {
    args.requireDefined(item.id, 'item.id')
    args.requireDefined(item.env, 'item.env')

    return item.id + item.env
  }
}

module.exports = MockRepo
