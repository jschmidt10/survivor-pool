'use strict'

const args = require('./args')

const config = {}

config.validate = function (config) {
  args.requireDefined(config, 'config')
  args.requireDefined(config.env, 'config.env')
  args.requireDefined(config.table, 'config.table')
}

module.exports = config
