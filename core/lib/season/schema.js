'use strict'

const config = require('../config')
const validator = require('./validator')

const SEASON_ID = 'SEASON'

const schema = {}

schema.SEASON_ID = SEASON_ID

/*
 * Dynamo GET for the season.
 */
schema.getRequest = function (c) {
  config.validate(c)

  return {
    Key: {
      id: schema.SEASON_ID,
      env: c.env
    },
    TableName: c.table
  }
}

/*
 * Dynamo PUT request for the current season.
 */
schema.putRequest = function (c, s) {
  config.validate(c)
  validator.validate(s)

  return {
    TableName: c.table,
    Item: {
      id: SEASON_ID,
      env: c.env,
      ...s
    }
  }
}

/*
 * Extracts the season from the Dynamo result.
 */
schema.fromDynamo = function (dynamoItem) {
  validator.validate(dynamoItem)

  return {
    name: dynamoItem.name,
    contestants: dynamoItem.contestants
  }
}

module.exports = schema
