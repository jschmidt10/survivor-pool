'use strict'

const args = require('../args')

const ArgumentError = args.ArgumentError
const contestantStatuses = ['active', 'eliminated']

const validator = {}

/*
 * Validates a season has all required fields.
 */
validator.validate = function validateSeason (season) {
  args.requireDefined(season, 'season')
  args.requireNonBlank(season.name, 'season.name')
  args.requireDefined(season.contestants, 'season.contestants')

  for (const contestant of season.contestants) {
    validateContestant(contestant)
  }
}

function validateContestant (contestant) {
  args.requireDefined(contestant, 'contestant')
  args.requireNonBlank(contestant.name, 'contestant.name')
  args.requireNonBlank(contestant.pic, 'contestant.pic')
  args.requireNonBlank(contestant.status, 'contestant.status')

  if (!contestantStatuses.includes(contestant.status)) {
    throw new ArgumentError(`Expected status to be one of ${contestantStatuses} but received '${contestant.status}'`)
  }
}

module.exports = validator
