'use strict'

const schema = require('./schema')
const validator = require('./validator')

const season = {}

/*
 * Validates a season domain object.
 */
season.validate = validator.validate

/*
 * Fetches the current season or null if no season exists.
 */
season.fetch = async function (config, repo) {
  const dynamoResult = await repo.get(schema.getRequest(config))

  if (dynamoResult && dynamoResult.Item) {
    return schema.fromDynamo(dynamoResult.Item)
  } else {
    return null
  }
}

/*
 * Deletes the current season.
 */
season.delete = function (config, repo) {
  return repo.delete(schema.getRequest(config))
}

/*
 * Saves the given season.
 */
season.save = function (config, repo, newSeason) {
  validator.validate(newSeason)
  return repo.put(schema.putRequest(config, newSeason))
}

module.exports = season
