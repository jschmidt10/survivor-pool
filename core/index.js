'use strict'

const args = require('./lib/args')
const pool = require('./lib/pool')
const season = require('./lib/season')

const AppError = require('./lib/app-error')
const MockRepo = require('./lib/mock-repo')

const ERROR_NO_SEASON = 'No season exists. The site maintainer needs to setup the current season before you can continue'
const ERROR_MISSING_CONTESTANT = 'The pool must contain all contestants from the current season'
const ERROR_INVALID_CONTESTANT = 'Found unknown contestants'
const ERROR_NOT_ENOUGH_PLAYERS = 'The pool must have at least two players'
const ERROR_EMPTY_PLAYER = 'All players must have at least one contestant'

const survivorpool = {}

survivorpool.args = args

survivorpool.AppError = AppError
survivorpool.MockRepo = MockRepo

survivorpool.ERROR_NO_SEASON = ERROR_NO_SEASON
survivorpool.ERROR_MISSING_CONTESTANT = ERROR_MISSING_CONTESTANT
survivorpool.ERROR_INVALID_CONTESTANT = ERROR_INVALID_CONTESTANT
survivorpool.ERROR_NOT_ENOUGH_PLAYERS = ERROR_NOT_ENOUGH_PLAYERS
survivorpool.ERROR_EMPTY_PLAYER = ERROR_EMPTY_PLAYER

/*
 * Fetches the current season
 */
survivorpool.fetchSeason = season.fetch

/*
 * Adds a new pool to the system
 */
survivorpool.addNewPool = async function (config, repo, newPool) {
  const insertPool = { ...newPool }
  insertPool.url = encodeURI(newPool.name)

  pool.validate(insertPool)
  const s = await ensureSeason(config, repo)
  validatePoolWithSeason(insertPool, s)

  return pool.save(config, repo, insertPool)
}

async function ensureSeason (config, repo) {
  const s = await season.fetch(config, repo)
  if (s === null) {
    throw new AppError(ERROR_NO_SEASON)
  }
  return s
}

function validatePoolWithSeason (newPool, s) {
  validateAllContestantsAssigned(newPool, s)
  validateNoExtraContestants(newPool, s)
  validateEnoughPlayers(newPool)
  validateNoEmptyPlayers(newPool)
}

function validateAllContestantsAssigned (newPool, s) {
  const assigned = newPool.players.flatMap(p => (p.contestants)).map(c => c.name)
  for (const c of s.contestants) {
    if (!assigned.includes(c.name)) {
      throw new AppError(ERROR_MISSING_CONTESTANT)
    }
  }
}

function validateNoExtraContestants (newPool, s) {
  const seasonContestants = s.contestants.map(c => c.name)
  const assigned = newPool.players.flatMap(p => (p.contestants)).map(c => c.name)

  for (const a of assigned) {
    if (!seasonContestants.includes(a)) {
      throw new AppError(ERROR_INVALID_CONTESTANT)
    }
  }
}

function validateEnoughPlayers (newPool) {
  if (newPool.players.length < 2) {
    throw new AppError(ERROR_NOT_ENOUGH_PLAYERS)
  }
}

function validateNoEmptyPlayers (newPool) {
  for (const player of newPool.players) {
    if (player.contestants.length === 0) {
      throw new AppError(ERROR_EMPTY_PLAYER)
    }
  }
}

/*
 * Fetches a pool by name.
 */
survivorpool.fetchPoolByName = async function (config, repo, poolName) {
  const p = await pool.fetchByName(config, repo, poolName)

  if (p === null) {
    return null
  } else {
    const s = await ensureSeason(config, repo)
    return addContestantInfo(p, s)
  }
}

function addContestantInfo (p, s) {
  const contestantLookup = {}

  for (const contestant of s.contestants) {
    contestantLookup[contestant.name] = contestant
  }

  for (const player of p.players) {
    for (const contestant of player.contestants) {
      const lookup = contestantLookup[contestant.name]
      if (lookup === undefined) {
        throw new AppError(ERROR_INVALID_CONTESTANT)
      }
      Object.assign(contestant, lookup)
    }
  }

  return p
}

/*
 * Update a contestant's status
 */
survivorpool.updateContestantStatus = async function (config, repo, name, status) {
  args.requireNonBlank(name)
  args.requireNonBlank(status)

  const s = await ensureSeason(config, repo)
  const contestant = s.contestants.find(c => c.name === name)
  if (contestant === undefined) {
    throw new AppError(ERROR_INVALID_CONTESTANT)
  }

  contestant.status = status

  await season.save(config, repo, s)
}

/*
 * Creates a new season, removes the old one, and removes all old pools
 */
survivorpool.createNewSeason = async function (config, repo, newSeason) {
  season.validate(newSeason)

  const poolListings = await pool.fetchListing(config, repo)
  const deletePromises = []

  for (const listing of poolListings) {
    deletePromises.push(pool.delete(config, repo, listing.name))
  }

  deletePromises.push(season.delete(config, repo))

  await Promise.all(deletePromises)
  await season.save(config, repo, newSeason)
}

/*
 * Fetch a listing of all pools
 */
survivorpool.fetchPoolListing = pool.fetchListing

module.exports = survivorpool
