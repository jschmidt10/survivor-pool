'use strict'

const pool = require('../lib/pool')
const season = require('../lib/season')
const survivorpool = require('../')
const MockRepo = require('../lib/mock-repo')

const AppError = survivorpool.AppError

describe('survivorpool app', function () {
  const config = {
    env: 'unit test',
    table: 'table'
  }

  var testSeason
  var mockRepo
  var testPool

  beforeEach(async function () {
    mockRepo = new MockRepo()

    testSeason = {
      name: 'Season One',
      contestants: [
        {
          name: 'Tommy',
          pic: 'tommy.jpg',
          status: 'active'
        },
        {
          name: 'Tina',
          pic: 'tina.jpg',
          status: 'eliminated'
        }
      ]
    }

    testPool = {
      name: 'My First Pool',
      url: 'My%20First%20Pool',
      players: [
        {
          name: 'Player One',
          contestants: [
            {
              name: 'Tommy'
            }
          ]
        },
        {
          name: 'Player Two',
          contestants: [
            {
              name: 'Tina'
            }
          ]
        }
      ]
    }

    await season.save(config, mockRepo, testSeason)
  })

  it('should invalidate a pool missing contestants', async function (done) {
    const badPool = { ...testPool }
    badPool.players[0].contestants = []

    try {
      await survivorpool.addNewPool(config, mockRepo, badPool)
      done(new Error('Expecteded a failure but completed successfully'))
    } catch (err) {
      expect(err instanceof AppError).toBeTrue()
      done()
    }
  })

  it('should invalidate a pool with extra contestants', async function (done) {
    const badPool = { ...testPool }
    badPool.players[0].contestants.push({ name: 'Bob' })

    try {
      await survivorpool.addNewPool(config, mockRepo, badPool)
      done(new Error('Expecteded a failure but completed successfully'))
    } catch (err) {
      expect(err instanceof AppError).toBeTrue()
      done()
    }
  })

  it('should invalidate a pool with less than two players', async function (done) {
    const badPool = { ...testPool }
    badPool.players = [
      testPool.players[0]
    ]

    badPool.players[0].contestants.push(testPool.players[1].contestants[0])

    try {
      await survivorpool.addNewPool(config, mockRepo, badPool)
      done(new Error('Expecteded a failure but completed successfully'))
    } catch (err) {
      expect(err instanceof AppError).toBeTrue()
      done()
    }
  })

  it('should invalidate a pool with players that have no contestants', async function (done) {
    const badPool = { ...testPool }
    badPool.players = [
      testPool.players[0]
    ]

    badPool.players[0].contestants.push(testPool.players[1].contestants[0])
    badPool.players.push({ name: 'Empty Player', contestants: [] })

    try {
      await survivorpool.addNewPool(config, mockRepo, badPool)
      done(new Error('Expecteded a failure but completed successfully'))
    } catch (err) {
      expect(err instanceof AppError).toBeTrue()
      done()
    }
  })

  it('should add a new, valid pool', async function () {
    await survivorpool.addNewPool(config, mockRepo, testPool)

    expect(await pool.fetchByName(config, mockRepo, testPool.name)).toEqual(testPool)
  })

  it('should fetch the current season', async function () {
    expect(await survivorpool.fetchSeason(config, mockRepo)).toEqual(testSeason)
  })

  it('should merge a pool with the current season data', async function () {
    const expectedPool = testPool = {
      name: 'My First Pool',
      url: 'My%20First%20Pool',
      players: [
        {
          name: 'Player One',
          contestants: [
            {
              name: 'Tommy',
              pic: 'tommy.jpg',
              status: 'active'
            }
          ]
        },
        {
          name: 'Player Two',
          contestants: [
            {
              name: 'Tina',
              pic: 'tina.jpg',
              status: 'eliminated'
            }
          ]
        }
      ]
    }

    await survivorpool.addNewPool(config, mockRepo, testPool)

    expect(await survivorpool.fetchPoolByName(config, mockRepo, testPool.name)).toEqual(expectedPool)
  })

  it('should update a player status', async function () {
    const name = 'Tommy'
    const status = 'eliminated'

    await survivorpool.updateContestantStatus(config, mockRepo, name, status)
    const s = await survivorpool.fetchSeason(config, mockRepo)
    const newStatus = s.contestants.find(c => c.name === name).status

    expect(newStatus).toEqual(status)
  })

  it('should throw error when player is not found', async function (done) {
    const name = 'non existent player'
    const status = 'active'

    try {
      await survivorpool.updateContestantStatus(config, mockRepo, name, status)
      done(new Error('Expecteded a failure but completed successfully'))
    } catch (err) {
      expect(err instanceof AppError).toBeTrue()
      done()
    }
  })

  it('should throw error if status is not active or eliminated', async function (done) {
    const name = 'Tina'
    const status = 'junk'
    try {
      await survivorpool.updateContestantStatus(config, mockRepo, name, status)
      done(new Error('Expecteded a failure but completed successfully'))
    } catch (err) {
      expect(err instanceof AppError).toBeTrue()
      done()
    }
  })

  it('should create a new season and delete old pools', async function () {
    await survivorpool.addNewPool(config, mockRepo, testPool)

    const newSeason = {
      name: 'Season Two',
      contestants: [
        {
          name: 'Phil',
          pic: 'phil.jpg',
          status: 'active'
        }
      ]
    }

    await survivorpool.createNewSeason(config, mockRepo, newSeason)

    expect(await survivorpool.fetchSeason(config, mockRepo)).toEqual(newSeason)
    expect(await survivorpool.fetchPoolListing(config, mockRepo)).toEqual([])
  })
})
