'use strict'

const args = require('../../lib/args')
const schema = require('../../lib/season/schema')

const ArgumentError = args.ArgumentError

describe('season schema', function () {
  const config = {
    env: 'TEST',
    table: 'table'
  }

  const season = {
    name: 'Season One',
    contestants: [
      {
        name: 'Tommy',
        status: 'active',
        pic: 'tommy.jpg'
      },
      {
        name: 'Tina',
        status: 'active',
        pic: 'tina.jpg'
      }
    ]
  }

  it('should create a Dynamo GET request', function () {
    const expectedRequest = {
      Key: {
        id: schema.SEASON_ID,
        env: config.env
      },
      TableName: config.table
    }

    expect(schema.getRequest(config)).toEqual(expectedRequest)
  })

  it('should create a Dynamo PUT request', function () {
    const expectedRequest = {
      TableName: config.table,
      Item: {
        id: schema.SEASON_ID,
        env: config.env,
        ...season
      }
    }

    expect(schema.putRequest(config, season)).toEqual(expectedRequest)
  })

  it('should return a season object without Dynamo metadata', function () {
    const dynamoSeason = {
      id: schema.SEASON_ID,
      env: config.env,
      ...season
    }

    expect(schema.fromDynamo(dynamoSeason)).toEqual(season)
  })

  it('should throw ArgumentError on null input', function () {
    expect(() => schema.getRequest(null)).toThrowError(ArgumentError)
    expect(() => schema.getRequest(undefined)).toThrowError(ArgumentError)
    expect(() => schema.putRequest(null)).toThrowError(ArgumentError)
    expect(() => schema.putRequest(undefined)).toThrowError(ArgumentError)
  })

  it('should throw ArgumentError when season is missing required fields', function () {
    const badSeason = {
      ...season,
      name: undefined
    }

    expect(() => schema.fromDynamo(badSeason)).toThrowError(ArgumentError)
  })
})
