'use strict'

const args = require('../../lib/args')
const season = require('../../lib/season')
const schema = require('../../lib/season/schema')
const MockRepo = require('../../lib/mock-repo')

const ArgumentError = args.ArgumentError

describe('season', function () {
  var mockRepo

  const config = {
    env: 'unit test',
    table: 'some table'
  }

  const testSeason = {
    name: 'Season One',
    contestants: [
      {
        name: 'Tommy',
        status: 'active',
        pic: 'tommy.jpg'
      },
      {
        name: 'Tina',
        status: 'active',
        pic: 'tina.jpg'
      }
    ]
  }

  beforeEach(function () {
    mockRepo = new MockRepo()
  })

  it('should validate a complete season', function () {
    season.validate(testSeason)
  })

  it('should invalidate a season without a name or contestants', function () {
    var badSeason = { ...testSeason }
    badSeason.name = ''
    expect(() => season.validate(badSeason)).toThrowError(ArgumentError)

    badSeason = { ...season }
    badSeason.contestants = null
    expect(() => season.validate(badSeason)).toThrowError(ArgumentError)
  })

  it('should invalidate an unrecognized contestant status', function () {
    const badSeason = { ...testSeason }
    badSeason.contestants = [{ name: 'Tommy', status: 'unknown', pic: 'tommy.jpg' }]

    expect(() => season.validate(badSeason)).toThrowError(ArgumentError)
  })

  it('should fetch the current season from the repo', async function () {
    mockRepo.put(testPutRequest())
    expect(await season.fetch(config, mockRepo)).toEqual(testSeason)
  })

  function testPutRequest () {
    return {
      Item: {
        id: schema.SEASON_ID,
        env: config.env,
        ...testSeason
      }
    }
  }

  it('should delete the current season from the repo', async function () {
    await mockRepo.put(testPutRequest())
    await season.delete(config, mockRepo)
    expect(await season.fetch(config, mockRepo)).toBeNull()
  })

  it('should save a new season in the repo', async function () {
    await season.save(config, mockRepo, testSeason)
    expect(await season.fetch(config, mockRepo)).toEqual(testSeason)
  })

  it('should return null when no season exists', async function () {
    expect(await season.fetch(config, mockRepo)).toBeNull()
  })
})
