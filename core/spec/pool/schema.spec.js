'use strict'

const schema = require('../../lib/pool/schema')

describe('pool schema', function () {
  const testConfig = {
    env: 'unit test',
    table: 'table'
  }

  const testPool = {
    name: 'Test Pool',
    url: 'Test%20Pool',
    players: [
      {
        name: 'Player One',
        contestants: [
          {
            name: 'Tommy'
          },
          {
            name: 'Tina'
          }
        ]
      },
      {
        name: 'Player Two',
        contestants: [
          {
            name: 'Bob'
          }
        ]
      }
    ]
  }

  it('should create a Dynamo GET request', function () {
    const expectedRequest = {
      Key: {
        id: schema.POOL_ID_PREFIX + testPool.name,
        env: testConfig.env
      },
      TableName: testConfig.table
    }

    expect(schema.getPoolRequest(testConfig, testPool.name)).toEqual(expectedRequest)
  })

  it('should convert a Dynamo result into a pool', function () {
    const dynamoPool = { ...testPool }
    dynamoPool.id = schema.POOL_ID_PREFIX + dynamoPool.name
    dynamoPool.env = testConfig.env

    expect(schema.fromDynamoPool(dynamoPool)).toEqual(testPool)
  })

  it('should create a Dynamo GET request for full pool listing', function () {
    const expectedRequest = {
      TableName: testConfig.table,
      KeyConditionExpression: schema.ID_QUERY,
      ExpressionAttributeValues: {
        [schema.ID_PARAM]: schema.LIST_ID_PREFIX + testConfig.env
      }
    }

    expect(schema.getAllListingRequest(testConfig)).toEqual(expectedRequest)
  })

  it('should create a Dynamo GET request for a single pool listing', function () {
    const expectedRequest = {
      TableName: testConfig.table,
      Key: {
        id: schema.LIST_ID_PREFIX + testConfig.env,
        env: testPool.name
      }
    }

    expect(schema.getListingRequest(testConfig, testPool.name)).toEqual(expectedRequest)
  })

  it('should create a Dynamo PUT request for pool', function () {
    const expectedRequest = {
      TableName: testConfig.table,
      Item: {
        id: schema.POOL_ID_PREFIX + testPool.name,
        env: testConfig.env,
        ...testPool
      }
    }

    expect(schema.putPoolRequest(testConfig, testPool)).toEqual(expectedRequest)
  })

  it('should create a Dynamo PUT request for pool listing', function () {
    const expectedRequest = {
      TableName: testConfig.table,
      Item: {
        id: schema.LIST_ID_PREFIX + testConfig.env,
        env: testPool.name,
        name: testPool.name,
        url: testPool.url
      }
    }

    expect(schema.putListingRequest(testConfig, testPool)).toEqual(expectedRequest)
  })
})
