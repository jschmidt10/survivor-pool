'use strict'

const args = require('../../lib/args')
const pool = require('../../lib/pool')
const MockRepo = require('../../lib/mock-repo')

const ArgumentError = args.ArgumentError

describe('pool', function () {
  var mockRepo

  const config = {
    env: 'unit test',
    table: 'some table'
  }

  const testPool = {
    name: 'Test Pool',
    url: 'Test%20Pool',
    players: [
      {
        name: 'Player One',
        contestants: [
          {
            name: 'Tommy'
          },
          {
            name: 'Tina'
          }
        ]
      },
      {
        name: 'Player Two',
        contestants: [
          {
            name: 'Bob'
          }
        ]
      }
    ]
  }

  beforeEach(function () {
    mockRepo = new MockRepo()
  })

  it('should validate a complete pool', function () {
    pool.validate(testPool)
  })

  it('should invalidate a pool missing required fields', function () {
    // no name
    var badPool = {
      ...testPool,
      name: null
    }
    expect(() => pool.validate(badPool)).toThrowError(ArgumentError)

    // player with no name
    badPool = {
      ...testPool,
      players: [
        {
          contestants: []
        }
      ]
    }
    expect(() => pool.validate(badPool)).toThrowError(ArgumentError)

    // contestant with no name
    badPool = {
      ...testPool,
      players: [
        {
          name: 'Bob',
          contestants: [
            {
              name: undefined
            }
          ]
        }
      ]
    }

    expect(() => pool.validate(badPool)).toThrowError(ArgumentError)
  })

  it('should save pool with list entries and fetch them', async function () {
    const expectedListing = [
      {
        name: testPool.name,
        url: testPool.url
      }
    ]

    await pool.save(config, mockRepo, testPool)

    const fetchedPool = await pool.fetchByName(config, mockRepo, testPool.name)
    expect(fetchedPool).toEqual(testPool)

    const fetchedListing = await pool.fetchListing(config, mockRepo)
    expect(fetchedListing).toEqual(expectedListing)
  })

  it('should return null for unfound pools', async function () {
    expect(await pool.fetchByName(config, mockRepo, 'Not Found Name')).toBeNull()
  })

  it('should return empty listing when there are no pools', async function () {
    expect(await pool.fetchListing(config, mockRepo)).toEqual([])
  })

  it('should delete a pool', async function () {
    await pool.save(config, mockRepo, testPool)
    await pool.delete(config, mockRepo, testPool.name)

    const fetchedPool = await pool.fetchByName(config, mockRepo, testPool.name)
    expect(fetchedPool).toBeNull()
  })
})
