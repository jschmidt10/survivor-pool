#!/usr/bin/env bash

set -o errexit
set -o pipefail

declare basedir="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
cd ${basedir}/..

npx eslint index.js $@

find lib/ -name "*.js" -exec npx eslint {} $@ \;
find spec/ -name "*.js" -exec npx eslint {} $@ \;
