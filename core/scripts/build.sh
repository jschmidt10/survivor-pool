#!/usr/bin/env bash

set -o errexit
set -o pipefail

declare basedir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

cd ${basedir}/..
echo "Building $(pwd)"

echo "Downloading dependencies"
npm install
echo "Formatting"
npm run-script format
echo "Testing"
npm test
echo "Packing"
npm pack