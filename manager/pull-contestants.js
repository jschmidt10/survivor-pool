const request = require('request')
const cheerio = require('cheerio')

const season = {
  name: 'Winners at War',
  current: true,
  contestants: []
}

function onError (err) {
  console.log(JSON.stringify(err, null, 2))
}

function onComplete () {
  console.log(JSON.stringify(season, null, 4))
}

function parseContestantPage (url, successCallback, errorCallback) {
  request(url, {}, (err, resp, data) => {
    if (err) {
      errorCallback(err)
      return
    }

    const $ = cheerio.load(data)

    $("img[class='thumb lazy']").each((index, element) => {
      var fullName = element.attribs.alt
      var pic = element.attribs['data-src']
      var firstName = fullName.split(' ')[0]

      if (firstName !== 'Jeff') {
        season.contestants.push({
          status: 'active',
          name: firstName,
          pic
        })
      }
    })

    successCallback()
  })
}

parseContestantPage('https://www.cbs.com/shows/survivor/cast/', onComplete, onError)
