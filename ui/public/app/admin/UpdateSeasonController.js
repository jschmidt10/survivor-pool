const UpdateSeasonController = function (SeasonService) {
  const vm = this

  vm.selectedContestant = null
  vm.contestants = []
  vm.status = 'eliminated'
  vm.adminPassword = ''
  vm.errorMessage = null
  vm.resultMessage = null

  vm.submit = submit

  activate()

  function activate () {
    SeasonService.getSeason().then(populateContestants).catch(populateError)
  }

  function populateContestants (season) {
    vm.contestants = season.contestants.map(c => c.name)
    vm.selectedContestant = vm.contestants[0]
  }

  function populateError (err) {
    vm.errorMessage = err.message
    console.error(err)
  }

  function submit () {
    vm.errorMessage = null
    vm.resultMessage = null

    SeasonService
      .updateContestantStatus(vm.selectedContestant, vm.status, vm.adminPassword)
      .then(populateMessage)
      .catch(populateError)
  }

  function populateMessage (_result) {
    vm.resultMessage = `Successfully updated ${vm.selectedContestant}`
  }

  return vm
}

UpdateSeasonController.$inject = ['SeasonService']

module.exports = UpdateSeasonController
