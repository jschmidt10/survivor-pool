const angular = require('angular')
const UpdateSeasonController = require('./UpdateSeasonController')

const moduleName = 'app.admin'

angular
  .module(moduleName, [])
  .controller('UpdateSeasonController', UpdateSeasonController)

module.exports = moduleName
