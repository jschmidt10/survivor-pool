const angular = require('angular')
const SeasonService = require('./SeasonService')

const moduleName = 'app.season'

angular
  .module(moduleName, [])
  .factory('SeasonService', SeasonService)

module.exports = moduleName
