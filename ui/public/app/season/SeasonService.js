const SeasonService = function ($http, seasonEndpoint, updateStatusEndpoint) {
  const service = {}

  service.getSeason = function () {
    return $http.get(seasonEndpoint).then(result => result.data).catch(parseError)
  }

  service.updateContestantStatus = function (contestant, status, adminPassword) {
    const body = {
      contestant: contestant,
      status: status,
      adminPassword: adminPassword
    }

    return $http.post(updateStatusEndpoint, body).then(result => result.data).catch(parseError)
  }

  function parseError (error) {
    if (error instanceof Error) {
      return Promise.reject(error)
    } else {
      return Promise.reject(new Error(error.data.error))
    }
  }

  return service
}

SeasonService.$inject = ['$http', 'seasonEndpoint', 'updateStatusEndpoint']

module.exports = SeasonService
