const ListPoolsController = function (PoolService) {
  const vm = this

  vm.pools = []
  vm.err = null

  activate()

  function activate () {
    PoolService
      .listPools()
      .then(populatePools)
      .catch(populateError)
  }

  function populatePools (pools) {
    for (const pool of pools) {
      vm.pools.push(pool)
    }
  }

  function populateError (err) {
    vm.err = err.message
    console.error(err)
  }

  return vm
}

ListPoolsController.$inject = ['PoolService']

module.exports = ListPoolsController
