const PoolService = function ($http, poolEndpoint) {
  const service = {}

  service.getPool = function (poolName) {
    return $http.get(`${poolEndpoint}/${poolName}`).then(result => result.data).catch(parseError)
  }

  service.listPools = function () {
    return $http.get(poolEndpoint).then(result => result.data).catch(parseError)
  }

  service.createPool = function (pool) {
    return $http.post(poolEndpoint, pool).catch(parseError)
  }

  function parseError (error) {
    if (error instanceof Error) {
      return Promise.reject(error)
    } else {
      return Promise.reject(new Error(error.data.error))
    }
  }

  return service
}

PoolService.$inject = ['$http', 'poolEndpoint']

module.exports = PoolService
