const CreatePoolController = function ($window, PoolService, SeasonService) {
  const vm = this

  vm.pool = {
    name: '',
    players: []
  }
  vm.err = null

  vm.newPlayer = ''
  vm.unassignedContestants = []
  vm.selectedContestant = null

  vm.submit = submit
  vm.addPlayer = addPlayer
  vm.removePlayer = removePlayer
  vm.contestantMoved = contestantMoved
  vm.contestantSelected = contestantSelected
  vm.isSelectedContestant = isSelectedContestant
  vm.playerContestantMoved = playerContestantMoved

  activate()

  function activate () {
    SeasonService.getSeason().then(populateContestants).catch(populateError)
  }

  function populateContestants (season) {
    vm.unassignedContestants = season.contestants
  }

  function populateError (err) {
    vm.err = err.message
    console.error(err)
  }

  function contestantMoved (index) {
    vm.unassignedContestants.splice(index, 1)
  }

  function contestantSelected (contestant) {
    vm.selectedContestant = contestant
  }

  function isSelectedContestant (contestant) {
    return vm.selectedContestant === contestant
  }

  function playerContestantMoved (player, index) {
    player.contestants.splice(index, 1)
  }

  function addPlayer () {
    if (vm.newPlayer.length > 0 && !playerAlreadyAdded(vm.newPlayer)) {
      vm.pool.players.push({
        name: vm.newPlayer,
        contestants: []
      })
    }
    vm.newPlayer = ''
  }

  function playerAlreadyAdded (playerName) {
    return vm.pool.players.find(p => p.name === playerName)
  }

  function removePlayer (playerName) {
    const player = vm.pool.players.find(p => p.name === playerName)

    if (player) {
      var index = vm.pool.players.indexOf(player)
      vm.unassignedContestants = vm.unassignedContestants.concat(player.contestants)
      vm.pool.players.splice(index, 1)
    }
  }

  function submit () {
    PoolService.createPool(vm.pool).then(forwardToPoolPage).catch(populateError)
  }

  function forwardToPoolPage (_result) {
    $window.location.href = '#/pool/' + encodeURI(vm.pool.name)
  }

  return vm
}

CreatePoolController.$inject = ['$window', 'PoolService', 'SeasonService']

module.exports = CreatePoolController
