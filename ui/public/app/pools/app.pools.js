const angular = require('angular')

// angular-drag-and-drop-lists does not export anything
require('angular-drag-and-drop-lists')

const PoolService = require('./PoolService')
const CreatePoolController = require('./CreatePoolController')
const ListPoolsController = require('./ListPoolsController')
const ViewPoolController = require('./ViewPoolController')

const moduleName = 'app.pools'

angular
  .module(moduleName, ['dndLists'])
  .factory('PoolService', PoolService)
  .controller('CreatePoolController', CreatePoolController)
  .controller('ListPoolsController', ListPoolsController)
  .controller('ViewPoolController', ViewPoolController)

module.exports = moduleName
