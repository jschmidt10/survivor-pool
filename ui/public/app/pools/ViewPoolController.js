const ViewPoolController = function ($routeParams, PoolService) {
  const vm = this

  vm.pool = {}
  vm.err = null
  vm.maxContestantsPerPlayer = 1

  const poolName = $routeParams.name

  activate()

  function activate () {
    if (poolName) {
      PoolService
        .getPool(encodeURI(poolName))
        .then(populatePool)
        .catch(populateError)
    } else {
      populateError(new Error('You must give a pool name'))
    }
  }

  function populatePool (pool) {
    vm.pool = pool
    vm.maxContestantsPerPlayer = Math.max.apply(null, pool.players.map(p => p.contestants.length))
  }

  function populateError (err) {
    vm.err = err.message
    console.error(err)
  }

  return vm
}

ViewPoolController.$inject = ['$routeParams', 'PoolService']

module.exports = ViewPoolController
