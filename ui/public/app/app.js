const angular = require('angular')
const ngRoute = require('angular-route')
const adminModule = require('./admin/app.admin')
const poolModule = require('./pools/app.pools')
const seasonModule = require('./season/app.season')

angular
  .module('app', [ngRoute, adminModule, poolModule, seasonModule])
  .constant('seasonEndpoint', '{{ROOT_URL}}/season')
  .constant('updateStatusEndpoint', '{{ROOT_URL}}/season/update')
  .constant('poolEndpoint', '{{ROOT_URL}}/pool')
  .config(['$routeProvider', '$locationProvider', addRoutes])

function addRoutes ($routeProvider, $locationProvider) {
  $locationProvider.hashPrefix('')
  $routeProvider
    .when('/admin/update', {
      templateUrl: 'app/admin/UpdateSeason.html',
      controller: 'UpdateSeasonController',
      controllerAs: 'vm'
    })
    .when('/pool/create', {
      templateUrl: 'app/pools/CreatePool.html',
      controller: 'CreatePoolController',
      controllerAs: 'vm'
    })
    .when('/pool/:name', {
      templateUrl: 'app/pools/ViewPool.html',
      controller: 'ViewPoolController',
      controllerAs: 'vm'
    })
    .when('/pools', {
      templateUrl: 'app/pools/ListPools.html',
      controller: 'ListPoolsController',
      controllerAs: 'vm'
    })
    .otherwise({
      templateUrl: 'app/intro.html'
    })
}
