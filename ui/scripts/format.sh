#!/usr/bin/env bash

set -o errexit
set -o pipefail

declare basedir="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
cd ${basedir}/..

find public/app -type f -name "*.js" -exec npx eslint {} $@ \;
