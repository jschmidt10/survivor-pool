#!/usr/bin/env bash

set -o errexit
set -o pipefail

declare basedir="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

cd ${basedir}/.. 

if [[ $# -ne 1 ]]
then
  echo "Missing required argument: <environment> (test or prod)"
  exit 1
fi

source ../scripts/build-env.sh

environment="$1"
appJs="dist/public/js/survivorpool.js"
urlToken="{{ROOT_URL}}"
replaceUrl=""

if [[ "${environment}" == "test" ]]
then
  replaceUrl="${TEST_API_GATEWAY_URL}"
elif [[ "${environment}" == "prod" ]]
then
  replaceUrl="${PROD_API_GATEWAY_URL}"
else
  echo "Environment must either be 'test' or 'prod'"
  exit 1
fi

if ! grep -q "${urlToken}" ${appJs}
then
  echo "The replacement token ${urlToken} is not in ${appJs}"
  echo "You should probably rebuild"
  exit 1
fi

echo "Replacing ${urlToken} with ${replaceUrl}"

if [[ $(uname) == "Darwin" ]]
then
  sed -i '' -e "s#${urlToken}#${replaceUrl}#g" "${appJs}"
else
  sed -i -e "s#${urlToken}#${replaceUrl}#g" "${appJs}"
fi

echo "Updated to ${environment}"