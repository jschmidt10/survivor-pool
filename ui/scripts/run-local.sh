#!/usr/bin/env bash

set -o errexit
set -o pipefail

declare basedir="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
cd ${basedir}/..

if [[ ! -f dist/public/index.html ]]
then
  echo "Did not find dist/public/index.html. You need to run the build first."
  exit 1
fi

SERVER_CMD=${basedir}/../node_modules/.bin/static-server

if [[ ! -x ${SERVER_CMD} ]]
then
  echo "Did not find the expected executable: ${SERVER_CMD}"
  exit 1
fi

cd dist/

${SERVER_CMD} 