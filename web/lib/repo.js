'use strict'

const AWS = require('aws-sdk')
const args = require('survivorpool').args

/*
 * A repo which delegates to the AWS DynamoDB document client
 */
class DynamoRepo {
  constructor (config) {
    args.requireDefined(config, 'config')
    args.requireDefined(config.region, 'config.region')

    AWS.config.update({ region: config.region })
    this.docClient = new AWS.DynamoDB.DocumentClient({ apiVersion: '2012-08-10' })
  }

  get (getRequest) {
    return this.docClient.get(getRequest).promise()
  }

  put (putRequest) {
    return this.docClient.put(putRequest).promise()
  }

  query (queryRequest) {
    return this.docClient.query(queryRequest).promise()
  }

  delete (deleteRequest) {
    return this.docClient.delete(deleteRequest).promise()
  }
}

module.exports = DynamoRepo
