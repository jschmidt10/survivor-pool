'use strict'

const request = {}

/*
 * Creates a new API Gateway Request object
 */
request.create = function (httpMethod, path, body) {
  return {
    httpMethod: httpMethod,
    pathParameters: {
      proxy: path
    },
    body: JSON.stringify(body)
  }
}

/*
 * Extract the path from an AWS API Gateway request
 */
request.getPath = function (req) {
  if (req.pathParameters) {
    return req.pathParameters.proxy
  } else {
    return undefined
  }
}

/*
 * Extract the http method from an AWS API Gateway request
 */
request.getHttpMethod = function (req) {
  return req.httpMethod
}

/*
 * Extract the body from an AWS API Gateway request
 */
request.getBody = function (req) {
  if (req.body) {
    return JSON.parse(req.body)
  } else {
    return null
  }
}

/*
 * Extracts the query string parameters from an AWS API Gateway request
 */
request.getQueryParameters = function (req) {
  return req.queryStringParameters
}

module.exports = request
