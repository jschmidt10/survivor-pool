'use strict'

const request = require('../request')
const response = require('../response')
const survivorpool = require('survivorpool')
const args = survivorpool.args

const pool = {}

const POOL_PATH = 'pool'

pool.POOL_PATH = POOL_PATH

/*
 * Handle pool requests
 */
pool.handle = async function (config, repo, req) {
  args.requireDefined(req, 'req')

  const path = request.getPath(req)
  const httpMethod = request.getHttpMethod(req)

  if (path === POOL_PATH) {
    if (httpMethod === 'GET') {
      return await listPools(config, repo)
    } else if (httpMethod === 'POST') {
      return await createPool(config, repo, request.getBody(req))
    } else {
      return response.create(405, {})
    }
  } else if (path.startsWith(POOL_PATH)) {
    if (httpMethod === 'GET') {
      return await getPool(config, repo, path)
    } else {
      return response.create(405, {})
    }
  } else {
    return response.create(404, {})
  }
}

async function listPools (config, repo) {
  const listing = await survivorpool.fetchPoolListing(config, repo)
  return response.create(200, listing)
}

async function getPool (config, repo, path) {
  const poolUrl = path.substring(path.indexOf('/', 2) + 1)
  const poolName = decodeURI(poolUrl)
  const pool = await survivorpool.fetchPoolByName(config, repo, poolName)

  if (pool === null) {
    return response.create(204, {})
  } else {
    return response.create(200, pool)
  }
}

async function createPool (config, repo, body) {
  await survivorpool.addNewPool(config, repo, body)
  return response.create(200, body)
}

module.exports = pool
