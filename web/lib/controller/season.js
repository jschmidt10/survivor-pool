'use strict'

const request = require('../request')
const response = require('../response')
const survivorpool = require('survivorpool')
const args = survivorpool.args

const season = {}

const SEASON_PATH = 'season'
const UPDATE_PATH = 'season/update'
const NEW_PATH = 'season/new'

const ERROR_ADMIN_PASSWORD = 'Incorrect admin password'

season.SEASON_PATH = SEASON_PATH

/*
 * Handle season requests
 */
season.handle = async function (config, repo, req) {
  args.requireDefined(req, 'req')

  const path = request.getPath(req)
  const httpMethod = request.getHttpMethod(req)
  const body = request.getBody(req)

  let knownPath = false

  if (path === SEASON_PATH) {
    knownPath = true
    if (httpMethod === 'GET') {
      return await fetchSeason(config, repo)
    }
  } else if (path === UPDATE_PATH) {
    knownPath = true
    if (httpMethod === 'POST') {
      return await updateSeason(config, repo, body)
    }
  } else if (path === NEW_PATH) {
    knownPath = true
    if (httpMethod === 'POST') {
      return await createNewSeason(config, repo, body)
    }
  }

  if (knownPath) {
    return response.create(405, {})
  } else {
    return response.create(404, {})
  }
}

async function fetchSeason (config, repo) {
  const s = await survivorpool.fetchSeason(config, repo)
  if (s === null) {
    return response.create(204, {})
  } else {
    return response.create(200, s)
  }
}

async function updateSeason (config, repo, body) {
  args.requireDefined(body, 'body')
  args.requireDefined(body.adminPassword, 'body.adminPassword')
  args.requireDefined(body.contestant, 'body.contestant')
  args.requireDefined(body.status, 'body.status')

  if (body.adminPassword === config.adminPassword) {
    await survivorpool.updateContestantStatus(config, repo, body.contestant, body.status)
    return response.create(200, {})
  } else {
    return response.create(400, {
      error: ERROR_ADMIN_PASSWORD
    })
  }
}

async function createNewSeason (config, repo, body) {
  args.requireDefined(body, 'body')
  args.requireDefined(body.adminPassword, 'body.adminPassword')

  if (body.adminPassword === config.adminPassword) {
    await survivorpool.createNewSeason(config, repo, body)
    return response.create(200, {})
  } else {
    return response.create(400, {
      error: ERROR_ADMIN_PASSWORD
    })
  }
}

module.exports = season
