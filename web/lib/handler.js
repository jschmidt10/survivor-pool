'use strict'

const request = require('./request')
const response = require('./response')

const poolController = require('./controller/pool')
const seasonController = require('./controller/season')

const AppError = require('survivorpool').AppError

const ERROR_SERVERSIDE = 'Something went wrong on our side. Please try again later.'

const handler = {}

handler.handle = async function (config, repo, req) {
  const path = request.getPath(req)
  if (path.startsWith(poolController.POOL_PATH)) {
    try {
      return await poolController.handle(config, repo, req)
    } catch (error) {
      return processError(error)
    }
  } else if (path.startsWith(seasonController.SEASON_PATH)) {
    try {
      return await seasonController.handle(config, repo, req)
    } catch (error) {
      return processError(error)
    }
  } else {
    return response.create(404, {})
  }
}

function processError (error) {
  console.error(error)
  if (error instanceof AppError) {
    return response.create(400, { error: error.message })
  } else {
    return response.create(500, { error: ERROR_SERVERSIDE })
  }
}

module.exports = handler
