'use strict'

const request = require('../lib/request')

describe('request', function () {
  const httpMethod = 'GET'
  const path = '/some/path'
  const body = { name: 'data' }
  const bodyStr = JSON.stringify(body)
  const queryParams = {
    name: 'Tom',
    age: 30
  }

  const sampleRequest = {
    httpMethod: httpMethod,
    pathParameters: {
      proxy: path
    },
    body: bodyStr,
    queryStringParameters: queryParams
  }

  it('should parse request pieces', function () {
    expect(request.getHttpMethod(sampleRequest)).toEqual(httpMethod)
    expect(request.getPath(sampleRequest)).toEqual(path)
    expect(request.getBody(sampleRequest)).toEqual(body)
    expect(request.getQueryParameters(sampleRequest)).toEqual(queryParams)
  })
})
