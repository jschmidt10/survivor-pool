'use strict'

const poolController = require('../../lib/controller/pool')
const request = require('../../lib/request')
const survivorpool = require('survivorpool')
const MockRepo = survivorpool.MockRepo

describe('pool controller', function () {
  const config = {
    env: 'unit test',
    table: 'table'
  }

  var testSeason
  var mockRepo
  var testPool

  beforeEach(async function () {
    mockRepo = new MockRepo()

    testSeason = {
      name: 'Season One',
      contestants: [
        {
          name: 'Tommy',
          pic: 'tommy.jpg',
          status: 'active'
        },
        {
          name: 'Tina',
          pic: 'tina.jpg',
          status: 'eliminated'
        }
      ]
    }

    testPool = {
      name: 'Some Pool',
      url: 'Some%20Pool',
      players: [
        {
          name: 'Player One',
          contestants: [
            {
              name: 'Tommy'
            }
          ]
        },
        {
          name: 'Player Two',
          contestants: [
            {
              name: 'Tina'
            }
          ]
        }
      ]
    }

    await survivorpool.createNewSeason(config, mockRepo, testSeason)
    await survivorpool.addNewPool(config, mockRepo, testPool)
  })

  it('should fetch pool listings', async function () {
    const fetchListingRequest = request.create('GET', 'pool')

    const expectedBody = JSON.stringify([
      {
        name: testPool.name,
        url: testPool.url
      }
    ])

    const resp = await poolController.handle(config, mockRepo, fetchListingRequest)

    expect(resp.statusCode).toEqual(200)
    expect(resp.body).toEqual(expectedBody)
  })

  it('should get a pool by name', async function () {
    const getRequest = request.create('GET', 'pool/' + testPool.url)

    const resp = await poolController.handle(config, mockRepo, getRequest)

    expect(resp.statusCode).toEqual(200)
    expect(resp.body).toEqual(JSON.stringify(testPool))
  })

  it('should return No Content for an unfound pool', async function () {
    const getRequest = request.create('GET', 'pool/junk')

    const resp = await poolController.handle(config, mockRepo, getRequest)

    expect(resp.statusCode).toEqual(204)
  })

  it('should create a new pool', async function () {
    const anotherPool = {
      name: 'AnotherPool',
      url: 'AnotherPool',
      players: [
        {
          name: 'Another Player One',
          contestants: [
            {
              name: 'Tommy'
            }
          ]
        },
        {
          name: 'Another Player Two',
          contestants: [
            {
              name: 'Tina'
            }
          ]
        }
      ]
    }

    const expectedMergedPool = {
      name: 'AnotherPool',
      url: 'AnotherPool',
      players: [
        {
          name: 'Another Player One',
          contestants: [
            {
              name: 'Tommy',
              pic: 'tommy.jpg',
              status: 'active'
            }
          ]
        },
        {
          name: 'Another Player Two',
          contestants: [
            {
              name: 'Tina',
              pic: 'tina.jpg',
              status: 'eliminated'
            }
          ]
        }
      ]
    }

    const createRequest = request.create('POST', 'pool', anotherPool)

    await poolController.handle(config, mockRepo, createRequest)

    const fetchedPool = await survivorpool.fetchPoolByName(config, mockRepo, anotherPool.name)

    expect(fetchedPool).toEqual(expectedMergedPool)
  })

  it('should return Method Not Allowed for /pool with different method', async function () {
    const req = request.create('DELETE', 'pool')

    const resp = await poolController.handle(config, mockRepo, req)

    expect(resp.statusCode).toEqual(405)
  })

  it('should return Page Not Found for unrecognized url', async function () {
    const req = request.create('DELETE', 'unknown')

    const resp = await poolController.handle(config, mockRepo, req)

    expect(resp.statusCode).toEqual(404)
  })
})
