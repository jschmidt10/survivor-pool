'use strict'

const request = require('../../lib/request')
const seasonController = require('../../lib/controller/season')
const survivorpool = require('survivorpool')
const MockRepo = survivorpool.MockRepo

describe('season controller', function () {
  const config = {
    env: 'unit test',
    table: 'table',
    adminPassword: 'TEST'
  }

  var testSeason
  var mockRepo

  beforeEach(async function () {
    mockRepo = new MockRepo()

    testSeason = {
      name: 'Season One',
      contestants: [
        {
          name: 'Tommy',
          pic: 'tommy.jpg',
          status: 'active'
        },
        {
          name: 'Tina',
          pic: 'tina.jpg',
          status: 'eliminated'
        }
      ]
    }

    await survivorpool.createNewSeason(config, mockRepo, testSeason)
  })

  it('should fetch the current season', async function () {
    const getRequest = request.create('GET', 'season')

    const resp = await seasonController.handle(config, mockRepo, getRequest)

    expect(resp.statusCode).toEqual(200)
    expect(resp.body).toEqual(JSON.stringify(testSeason))
  })

  it('should update current season contestant status', async function () {
    const updateBody = {
      adminPassword: config.adminPassword,
      contestant: 'Tina',
      status: 'active'
    }

    const updateRequest = request.create('POST', 'season/update', updateBody)

    const resp = await seasonController.handle(config, mockRepo, updateRequest)

    expect(resp.statusCode).toEqual(200)

    const updatedSeason = await survivorpool.fetchSeason(config, mockRepo)
    const updatedContestant = updatedSeason.contestants.find(c => c.name === updateBody.contestant)

    expect(updatedContestant.status).toEqual(updateBody.status)
  })

  it('should return Bad Request for invalid password', async function () {
    const updateBody = {
      adminPassword: 'BAD PASSWORD',
      contestant: 'Tina',
      status: 'active'
    }

    const updateRequest = request.create('POST', 'season/update', updateBody)

    const resp = await seasonController.handle(config, mockRepo, updateRequest)

    expect(resp.statusCode).toEqual(400)
  })

  it('should create a new season', async function () {
    const newSeason = {
      ...testSeason,
      name: 'NewSeason'
    }

    const createBody = {
      adminPassword: config.adminPassword,
      ...newSeason
    }

    const req = request.create('POST', 'season/new', createBody)
    const resp = await seasonController.handle(config, mockRepo, req)

    expect(resp.statusCode).toEqual(200)
    expect(await survivorpool.fetchSeason(config, mockRepo)).toEqual(newSeason)
  })
})
