'use strict'

const handler = require('./lib/handler')
const DynamoRepo = require('./lib/repo')

const config = {
  adminPassword: process.env.ADMIN_PASSWORD,
  table: process.env.TABLE,
  env: process.env.ENVIRONMENT,
  region: process.env.REGION
}

const repo = new DynamoRepo(config)

/*
 * AWS Lambda entry point
 */
module.exports.handler = async function (event, context) {
  return handler.handle(config, repo, event)
}
